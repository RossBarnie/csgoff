CS:GO Friend Finder [![Build Status](https://drone.io/bitbucket.org/RossBarnie/csgoff/status.png)](https://drone.io/bitbucket.org/RossBarnie/csgoff/latest)
===================

A web application to help players of Counter-Strike: Global Offensive find others to play with.


Current State
--------------
See the Trello board for planned features and current development status: https://trello.com/b/VG35F2gU/csgoff